exports.up = function (knex, Promise) {
    return knex.schema
        .createTable('warehouses', function (table) {
            table.increments('id').primary();
            table.string('warehouse_id')
            table.string('warehouse_name');
            table.string('country');
            table.string('location');
            table.string('address');
            table.boolean('is_active');
            table
                .timestamp('created_at')
                .notNullable()
                .defaultTo(knex.fn.now());
            table
                .timestamp('updated_at')
                .notNullable()
                .defaultTo(knex.fn.now());
        })
        .createTable('products', function (table) {
            table.increments('id').primary();
            table.string('product_id');
            table.string('product_name');
            table.string('product_category');
            table.string('warehouse_id');
            table.string('color');
            table.string('size');
            table.string('price');
            table.string('image_url');
            table.boolean('is_active');

            table
                .timestamp('created_at')
                .notNullable()
                .defaultTo(knex.fn.now());
            table
                .timestamp('updated_at')
                .notNullable()
                .defaultTo(knex.fn.now());
        })

};

exports.down = function (knex, Promise) {
    return knex.schema.dropTable('subMaterial').dropTable('material');
};

