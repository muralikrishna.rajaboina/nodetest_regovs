exports.up = function (knex) {
    return Promise.all([
        knex.schema.createTable('Users', function (table) {
            table.increments('id').primary();
            table.string('userName');
            table.string('emailId');
            table.string('password');
            // table.string('phoneNumber');
            table.string('role');
            table.boolean('isActive');
            table
                .timestamp('created_at')
                .notNullable()
                .defaultTo(knex.fn.now());
            table
                .timestamp('updated_at')
                .notNullable()
                .defaultTo(knex.fn.now());
        })
    ]);
};

exports.down = function (knex) {
    return Promise.all([knex.schema.dropTable('Users')]);
};

