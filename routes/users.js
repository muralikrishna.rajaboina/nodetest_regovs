var express = require('express');
const router = express.Router();
const knex = require('knex');
const dbConfig = require('../knexfile').development;
// var t = require('tcomb-validation');
// var validations =  require('./validations');
const auth = require('../authentication/auth')();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const LocalStrategy = require('passport-local').Strategy;
const cfg = require('../authentication/jwt_config.js');
const https = require('https');
const bcrypt = require('bcryptjs');
// const { URL } = require('url');
/* GET users listing. */
router.post('/register', async (req, res) => {

  let db;
  try {
    db = knex(dbConfig);

    const hashedPassword = bcrypt.hashSync(req.body.password, 10);
    const user = await db.returning('*')
      .insert({
        userName: req.body.userName,
        emailId: req.body.emailId,
        password: hashedPassword,
        role: req.body.role,
        isActive: true
      })
      .into('Users')

    await db.destroy();
    if (user) {
      res.status(200).json({ data: user[0], message: ' User added sucessfully' });
    }
    else {
      return res.status(500).json({ message: 'User not added' });
    }
  }
  catch (error) {
    await db.destroy();
    return res.status(500).json({ message: 'something wrong' });
  }
})

router.post('/signIn', async (req, res) => {

  let db;
  try {
    db = knex(dbConfig);
    if (req.body.emailId && req.body.password) {
      const user = await db
        .select("*")
        .from('Users')
        .where("emailId", req.body.emailId)
        .andWhere('isActive', true)

      if (bcrypt.compareSync(req.body.password, user[0].password)) {

        return res.status(200).json({
          token: jwt.sign({ userName: req.body.userName }, cfg.jwtSecret),
          userName: user[0].userName,
          role: user[0].role
        });
      } else {
        res.status(404).json('User not Existed');
      }
    } else {
      req.res.status(400).json('bad request')
    }
  } catch (error) {
    return res.status(500).json({
      message: error.message
    })
  }
});

module.exports = router;
