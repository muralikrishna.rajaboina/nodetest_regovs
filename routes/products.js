var express = require('express');
var router = express.Router();
const knex = require('knex');
const dbConfig = require('../knexfile').development;

// Add product details into warehouse table
router.post('/addproduct', async (req, res) => {

    let db;
    try {
        db = knex(dbConfig);

        const product = await db.returning('*')
            .insert({
                product_id: req.body.productId,
                product_name: req.body.productName,
                product_category: req.body.productCategory,
                warehouse_id: req.body.warehouseId,
                color: req.body.color,
                size: req.body.size,
                price: req.body.price,
                image_url: req.body.imageUrl,
                is_active: req.body.isActive
            })
            .into('products')
        if (product) {
            await db.destroy();
            return res.status(200).json({ message: "New Product added successfully" })
        }
        else {
            res.status(400).json("Bad Request")
        }

    }
    catch (error) {
        await db.destroy();
        return res.status(500).json({ message: 'something wrong' });
    }
})

//products list filter by warehouseid , productCategory,and productName
router.get('/list', (req, res) => {
    let db;

    db = knex(dbConfig);

    const pageSize = parseInt(req.query.pageSize);
    const pageIndex = parseInt(req.query.pageIndex);

    const warehouseList = db
        .select('*')
        .from('products')
        .where({ 'is_active': true })
        .limit(pageSize)
        .offset(pageSize * pageIndex)
        .orderBy('created_at', 'desc')

    if (req.query.warehouseId && req.query.warehouseId !== '') {
        warehouseList.where('warehouse_id', 'ilike', '%' + req.query.warehouseId + '%')

    }
    if (req.query.productCategory && req.query.productCategory !== '') {
        warehouseList.where('product_category', 'ilike', '%' + req.query.productCategory + '%')

    }
    if (req.query.productName && req.query.productName !== '') {
        warehouseList.where('product_name', 'ilike', '%' + req.query.productName + '%')

    }

    warehouseList.then(results => {
        db.destroy();

        return res.json(
            results
        );
    })
        .catch(e => {
            db.destroy();
            return res.status(500).json(e);
        });
});

router.delete('/delete/:productId', async (req, res) => {
    let db;
    console.log(req.params)
    try {
        db = knex(dbConfig);
        console.log(req.params)
        const deleteproduct = await db
            .from('products')
            .where({ product_id: req.params.productId })
            .del()
        await db.destroy();
        if (deleteproduct !== 0) {

            return res.status(200).json({ message: `product with id: ${req.params.productId} deleted successfully` })
        }
        else {
            return res.status(404).json({ message: `No product found with the ID: ${req.params.productId}` })
        }
    }

    catch (error) {
        await db.destroy();
        return res.status(500).json(error);
    };
});

module.exports = router;

