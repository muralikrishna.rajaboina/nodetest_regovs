var express = require('express');
var router = express.Router();
const knex = require('knex');
const dbConfig = require('../knexfile').development;

router.get('/warehousestock', async (req, res) => {
    let db;
    try {
        db = knex(dbConfig);
        const productsStock = await db.select(
            'products.warehouse_id as warehouseId',
            'warehouses.warehouse_name as warehouseName')
            .from('products')
            .groupBy(
                'products.warehouse_id',
                'warehouses.warehouse_name')
            .count('products.warehouse_id as warehouseStock')
            .innerJoin(
                'warehouses',
                'warehouses.warehouse_id',
                'products.warehouse_id'
            )
        await db.destroy();
        if (productsStock) {

            return res.status(200).json(productsStock)
        }
        else {
            return res.status(404).json({ message: `Not Found` })
        }
    }

    catch (error) {
        await db.destroy();
        return res.status(500).json(error);
    };
});

router.delete('/warehouseunstock', async (req, res) => {
    let db;
    try {
        db = knex(dbConfig);
        // postgresql does't support delete by limit so need to write sub query to get ctid or id's
        const products = await db.select('id')
            .from('products')
            .where({ warehouse_id: req.query.warehouseId }).orderBy('created_at', 'desc').limit(req.query.quantity)

        productsids = products.map(x => x.id)
        console.log(productsids)
        const productsUnstock = await db.from('products').select('*')
            .whereIn(
                'id', productsids
            ).del()
        console.log(productsUnstock)
        await db.destroy();
        if (productsUnstock !== 0) {

            return res.status(200).json({ message: `Warehouse producrs with id: ${req.query.warehouseId} deleted successfully` })
        }
        else {
            return res.status(404).json({ message: `No Warehouse products found with the ID: ${req.query.warehouseId}` })
        }
    }
    catch (error) {
        await db.destroy();
        return res.status(500).json(error);
    };
});

module.exports = router;
