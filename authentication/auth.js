let passport = require('passport');
let passportJWT = require('passport-jwt');
let cfg = require('../authentication/jwt_config.js');
const knex = require('knex');
const dbConfig = require('../knexfile').development;
let ExtractJWT = passportJWT.ExtractJwt;
let Strategy = passportJWT.Strategy;
let params = {
  secretOrKey: cfg.jwtSecret,
  jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken()
};
module.exports = function() {
  let strategy = new Strategy(params, function(payload, done) {
    const user = knex(dbConfig)
      .from('Users')
      .returning('*')
      .where({ userName: payload.userName });
    user.then(u => {
      cfg;
      //console.log(u);
      if (u) {
        return done(null, {
          id: u.id,
          userName: u.userName,
          phoneNumber: u.phoneNumber,
          role: u.role,
          isActive: u.isActive
        });
      } else {
        return done(new Error('User not found'), null);
      }
    });
  });
  passport.use(strategy);
  return {
    initialize: function() {
      return passport.initialize();
    },
    authenticate: function() {
      return passport.authenticate('jwt', cfg.jwtSession);
    }
  };
};
