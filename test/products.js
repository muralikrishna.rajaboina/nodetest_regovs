const test = require('ava');
const request = require('supertest');
const app = require('../app');

test('signIn:invalid body values', async t => {
    t.plan(2);
    const res = await request(app)
        .post('/users/signIn')
        .send({ emailId: "murali@gmail.com", password: "dd" });
    t.is(res.status, 404);
    t.deepEqual(res.body, 'User not Existed');
});
test('signIn:invalid body properties', async t => {
    t.plan(2);
    const res = await request(app)
        .post('/users/signIn')
        .send({ username: "murali@gmail.com", password: "dd" });
    t.is(res.status, 400);
    t.deepEqual(res.body, "bad request");
});

test('signIn: Something wrong Internal', async t => {
    t.plan(1);
    const res = await request(app)
        .post('/users/signIn')
        .send({ emailId: "muraffi@gmail.com", password: "dd" });
    t.is(res.status, 500);

});

test('signIn: Success', async t => {
    t.plan(1);
    const res = await request(app)
        .post('/users/signIn')
        .send({ emailId: "murali@gmail.com", password: "abc" });
    t.is(res.status, 200);


});
// Add product Successfully
test('Add product: Post', async t => {

    t.plan(1);
    const res = await request(app)
        .post('/products/addproduct')
        .send({
            "productId": "P12",
            "productName": "Leather Belt",
            "productCategory": "Belts",
            "warehouseId": "PWH2",
            "color": "Brown",
            "size": "40",
            "price": "100 RM",
            "imageUrl": "www.image12.com",
            "isActive": true
        })
    t.is(res.status, 200);
});

test('Products List:Get all or by filter', async t => {

    t.plan(1);
    const res = await request(app)
        .get('/products/list?pageSize=10&pageIndex=0&warehouseId=KL')

    t.is(res.status, 200);

});

//Add warehouse Successfully
test('Add warehouse: Post', async t => {

    t.plan(1);
    const res = await request(app)
        .post('/warehouses/addwarehouse')
        .send({
            "warehouseId": "W5",
            "warehouseName": "Cyberjaya Warehouse",
            "country": "Malaysia",
            "location": "Cyberjaya",
            "address": "Persiaran Bestari, Cyberjaya"

        })
    t.is(res.status, 200);
});

test('warehouses List:Get all', async t => {

    t.plan(1);
    const res = await request(app)
        .get('/warehouses/list?pageSize=3&pageIndex=0')

    t.is(res.status, 200);

});

test('warehouses stock:Get all', async t => {

    t.plan(1);
    const res = await request(app)
        .get('/warehousestock')

    t.is(res.status, 200);

});

test('warehouse unstock:Get all', async t => {

    t.plan(1);
    const res = await request(app)
        .delete('/warehouseunstock?warehouseId=KLWH1&quantity=1')

    t.is(res.status, 200);

});


