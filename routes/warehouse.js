var express = require('express');
var router = express.Router();
const knex = require('knex');
const dbConfig = require('../knexfile').development;

// Add warehouse details into warehouse table
router.post('/addwarehouse', async (req, res) => {

    let db;
    try {
        db = knex(dbConfig);

        const Warehouse = await db.returning('*')
            .insert({
                warehouse_id: req.body.warehouseId,
                warehouse_name: req.body.warehouseName,
                country: req.body.country,
                location: req.body.location,
                address: req.body.address,
                is_active: true
            })
            .into('warehouses')
        if (Warehouse) {
            await db.destroy();
            return res.status(200).json({ message: `${Warehouse[0].warehouse_name} added successfully ` })
        }
        else {
            res.status(400).json("Bad Request")
        }

    }
    catch (error) {
        await db.destroy();
        return res.status(500).json({ message: 'something wrong' });
    }
})

router.get('/list', async (req, res) => {
    let db;
    try {
        db = knex(dbConfig);

        const pageSize = parseInt(req.query.pageSize);
        const pageIndex = parseInt(req.query.pageIndex);

        const warehouseList = await db
            .select('*')
            .from('warehouses')
            .where({ 'is_active': true })
            .limit(pageSize)
            .offset(pageSize * pageIndex)
            .orderBy('created_at', 'asc')

        if (warehouseList) {
            await db.destroy();
            return res.status(200).json(warehouseList)
        }

        else {
            return res.status(404).json({ message: "No records found" })
        }
    }

    catch (error) {
        await db.destroy();
        return res.status(500).json(error);
    };
});

router.delete('/delete/:warehouseId', async (req, res) => {
    let db;
    console.log(req.params)
    try {
        db = knex(dbConfig);

        const deleteWarehouse = await db
            .from('warehouses')
            .where({ warehouse_id: req.params.warehouseId })
            .del()
        await db.destroy();
        if (deleteWarehouse !== 0) {

            return res.status(200).json({ message: `Warehouse with id: ${req.params.warehouseId} deleted successfully` })
        }
        else {
            return res.status(404).json({ message: `No Warehouse found with the ID: ${req.params.warehouseId}` })
        }
    }

    catch (error) {
        await db.destroy();
        return res.status(500).json(error);
    };
});

module.exports = router;
